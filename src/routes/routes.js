const router = require('express').Router()
const usuarioController = require('../controller/usuarioController');

router.post('/usuarioPost/', usuarioController.criarUsuario);
router.get('/usuarioGet/', usuarioController.buscarTodosUsuarios);
router.get('/usuarioGet/:id', usuarioController.buscarUsuarioPorId);
router.put('/usuarioAtualiza/:id', usuarioController.atualizarUsuario);
router.delete('/usuarioDeleta/:id', usuarioController.excluirUsuario);

module.exports = router; 
