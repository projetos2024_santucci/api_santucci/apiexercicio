const db = require('../database');
const bcrypt = require('bcrypt');

class UsuariosController {constructor() {}

  static async criarUsuario(req, res) {
    const { nome, email, senha, cpf, telefone } = req.body;

    if (
      nome === '' ||
      email === '' ||
      senha === '' ||
      cpf === '' ||
      telefone === ''
    ) {
      res.status(500).json({ error: 'Preencha todos os campos corretamente.' });
      return;
    }

    try {
      const hashedSenha = await bcrypt.hash(senha, 10);

      await db.query(
        'INSERT INTO Usuario (nome, email, senha, cpf, telefone) VALUES (?, ?, ?, ?, ?)',
        [nome, email, hashedSenha, cpf, telefone]
      );

      res.status(201).json({ message: 'Usuário criado com sucesso!' });
    } catch (error) {
      res.status(500).json({ error: 'Erro ao criar usuário: ' + error.message });
    }
  }

  static async buscarTodosUsuarios(req, res) {
    try {
      const usuarios = await db.query('SELECT * FROM Usuario');
      res.status(200).json(usuarios);
    } catch (error) {
      res.status(500).json({ error: 'Erro ao buscar usuários: ' + error.message });
    }
  }

  static async buscarUsuarioPorId(req, res) {
    const id = req.params.id;

    try {
      const usuario = await db.query('SELECT * FROM Usuario WHERE idUsuario = ?', [id]);

      if (usuario.length === 0) {
        return res.status(404).json({ error: 'Usuário não encontrado.' });
      }

      res.status(200).json(usuario[0]);
    } catch (error) {
      res.status(500).json({ error: 'Erro ao buscar usuário: ' + error.message });
    }
  }

  static async atualizarUsuario(req, res) {
    const id = req.params.id;
    const { nome, email, senha, cpf, telefone } = req.body;

    if (
      nome === '' ||
      email === '' ||
      senha === '' ||
      cpf === '' ||
      telefone === ''
    ) {
      res.status(500).json({ error: 'Preencha todos os campos corretamente.' });
      return;
    }

    try {
      const hashedSenha = await bcrypt.hash(senha, 10);

      await db.query(
        'UPDATE Usuario SET nome = ?, email = ?, senha = ?, cpf = ?, telefone = ? WHERE idUsuario = ?',
        [nome, email, hashedSenha, cpf, telefone, id]
      );

      res.status(200).json({ message: 'Usuário atualizado com sucesso!' });
    } catch (error) {
      res.status(500).json({ error: 'Erro ao atualizar usuário: ' + error.message });
    }
  }

  static async excluirUsuario(req, res) {
    const id = req.params.id;

    try {
      await db.query('DELETE FROM Usuario WHERE idUsuario = ?', [id]);
      res.status(200).json({ message: 'Usuário excluído com sucesso!' });
    } catch (error) {
      res.status(500).json({ error: 'Erro ao excluir usuário: ' + error.message });
    }
  }
}

module.exports = UsuariosController;
