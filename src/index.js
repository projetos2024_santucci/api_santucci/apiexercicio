const express = require('express')
const cors = require('cors')
const db = require('./database')

class ControllerApi {
    constructor()
    {
        this.express = express();
        this.middlewares();
        this.routes();
    }

    middlewares(){
        this.express.use(express.json())
        this.express.use(express(cors()))
    }

    routes()
    {
        const rotas = require('./routes/routes')
        this.express.use('/eventos/', rotas)
        function filtrarResposta(resposta) {
            // Verifique se a resposta é um objeto
            if (!resposta || typeof resposta !== 'object') {
              return resposta;
            }
          
            // Remova o buffer e outras propriedades irrelevantes
            const propriedadesParaRemover = ['_buf', '_clientEncoding', '_catalogLength', '_catalogStart', '_schemaLength', '_schemaStart', '_tableLength', '_tableStart', '_orgTableLength', '_orgTableStart', '_orgNameLength', '_orgNameStart', 'characterSet','encoding', 'name', 'columnLength', 'columnType', 'type', 'flags', 'decimals', '{}'               ];                                                                    
            propriedadesParaRemover.forEach((propriedade) => {
              if (Array.isArray(resposta)) {
                resposta = resposta.map((item) => {
                  delete item[propriedade];
                  return filtrarResposta(item);
                });
              } else {
                delete resposta[propriedade];
              }
            });
          
            // Retorne a resposta filtrada
            return resposta;
          }
          
          
          // Rota para mostrar as tabelas
          this.express.get('/mostrarTabelas/', (req, res) => {
            db.query('SHOW TABLES').then((resultado) => {
              const resultadoFiltrado = filtrarResposta(resultado);
              res.status(200).json(resultadoFiltrado);
            }).catch((erro) => {
              res.status(200).json({ error: 'Não foi possível conectar a base dos dados SQL.', erro });
            });
          });
          
          // Rotas para mostrar a descrição das tabelas
          this.express.get('/mostrarDesc1/', (req, res) => {
            db.query('DESC Usuario').then((resultado) => {
              const resultadoFiltrado = filtrarResposta(resultado);
              res.status(200).json(resultadoFiltrado);
            }).catch((erro) => {
              res.status(200).json({ error: 'Não foi possível conectar a base dos dados SQL.', erro });
            });
          });
          
          this.express.get('/mostrarDesc2/', (req, res) => {
            db.query('DESC Compra').then((resultado) => {
              const resultadoFiltrado = filtrarResposta(resultado);
              res.status(200).json(resultadoFiltrado);
            }).catch((erro) => {
              res.status(200).json({ error: 'Não foi possível conectar a base dos dados SQL.', erro });
            });
          });
          
          this.express.get('/mostrarDesc3/', (req, res) => {
            db.query('DESC Vendas').then((resultado) => {
              const resultadoFiltrado = filtrarResposta(resultado);
              res.status(200).json(resultadoFiltrado);
            }).catch((erro) => {
              res.status(200).json({ error: 'Não foi possível conectar a base dos dados SQL.', erro });
            });
          });
          
          this.express.get('/mostrarDesc4/', (req, res) => {
            db.query('DESC Organizador').then((resultado) => {
              const resultadoFiltrado = filtrarResposta(resultado);
              res.status(200).json(resultadoFiltrado);
            }).catch((erro) => {
              res.status(200).json({ error: 'Não foi possível conectar a base dos dados SQL.', erro });
            });
          });
          
          this.express.get('/mostrarDesc5/', (req, res) => {
            db.query('DESC Evento').then((resultado) => {
              const resultadoFiltrado = filtrarResposta(resultado);
              res.status(200).json(resultadoFiltrado);
            }).catch((erro) => {
              res.status(200).json({ error: 'Não foi possível conectar a base dos dados SQL.', erro });
            });
          });
          
          this.express.get('/mostrarDesc6/', (req, res) => {
            db.query('DESC Ingresso').then((resultado) => {
              const resultadoFiltrado = filtrarResposta(resultado);
              res.status(200).json(resultadoFiltrado);
            }).catch((erro) => {
              res.status(200).json({ error: 'Não foi possível conectar a base dos dados SQL.', erro });
            });
          });
          
    }
}

module.exports = new ControllerApi().express